# OpenML dataset: Toronto-COVID-19-Cases

https://www.openml.org/d/43480

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This data set contains demographic, geographic, and severity information for all confirmed and probable cases reported to and managed by Toronto Public Health since the first case was reported in January 2020. This includes cases that are sporadic (occurring in the community) and outbreak-associated. The data are extracted from the provincial communicable disease reporting system (iPHIS) and Toronto's custom COVID-19 case management system (CORES) and combined for reporting.
The data in this spreadsheet are subject to change as public health investigations into reported cases and continuous quality improvement initiatives are ongoing, and additional cases continue to be reported.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43480) of an [OpenML dataset](https://www.openml.org/d/43480). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43480/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43480/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43480/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

